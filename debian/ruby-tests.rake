File.delete("Gemfile.lock") if File.exist?("Gemfile.lock")
require 'bundler/setup'
require 'rspec/core/rake_task'

task :spec do
  ENV['DB_ADAPTER'] = 'sqlite3'
end

RSpec::Core::RakeTask.new(:spec) do |spec|
  spec.pattern = './spec/*_spec.rb'
end

task :default => :spec
